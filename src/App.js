import React, { useState } from 'react'
import Atomix from 'atomix-webuikit'

const getListItem = (id, label, hexColor, selectedId) => ({
  componentType: 'ListItemColorPalette',
  label: `${id} - ${label}`,
  colorPalette: [hexColor],
  colorsPaletteShape: 'square',
  colorsPaletteColumns: 14,
  isSelected: selectedId === id,
  metadata: {
    id
  }
})

const getListItems = selectedId => [
  getListItem(1, 'Red', '#FF0000', selectedId),
  getListItem(2, 'Green', '#00FF00', selectedId),
  getListItem(3, 'Blue', '#0000FF', selectedId)
]

const App = () => {
  const [selectedId, setSelectedId] = useState(2)
  
  return [
    <span>Selected ID: {selectedId}</span>,
    <Atomix.Organisms.Dropdown
      listItems={getListItems(selectedId)}
      onClick={listItem => setSelectedId(listItem.metadata.id)}
    />
  ]
}

export default App
